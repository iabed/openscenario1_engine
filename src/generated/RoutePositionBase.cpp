/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoutePositionBase.h"

#include "InRoutePosition.h"
#include "OpenScenarioEngineFactory.h"
#include "Orientation.h"
#include "RouteRef.h"

namespace OPENSCENARIO
{
RoutePositionBase::RoutePositionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRoutePosition> routePosition)
    : Node{"RoutePositionBase"}, routeRef_{(std::cout << "RoutePositionBase instantiating routeRef_" << std::endl, Builder::transform<RouteRef>(routePosition->GetRouteRef()))}, orientation_{(std::cout << "RoutePositionBase instantiating orientation_" << std::endl, Builder::transform<Orientation>(routePosition->GetOrientation()))}, inRoutePosition_{(std::cout << "RoutePositionBase instantiating inRoutePosition_" << std::endl, Builder::transform<InRoutePosition>(routePosition->GetInRoutePosition()))}

{
}

bool RoutePositionBase::Complete() const
{
    std::cout << "RoutePositionBase complete?\n";
    return OPENSCENARIO::Complete(routeRef_) && OPENSCENARIO::Complete(orientation_) && OPENSCENARIO::Complete(inRoutePosition_);
}

void RoutePositionBase::Step()
{
    std::cout << "RoutePositionBase step!\n";
    OPENSCENARIO::Step(routeRef_);
    OPENSCENARIO::Step(orientation_);
    OPENSCENARIO::Step(inRoutePosition_);
}

void RoutePositionBase::Stop()
{
    std::cout << "RoutePositionBase stop!\n";
    OPENSCENARIO::Stop(routeRef_);
    OPENSCENARIO::Stop(orientation_);
    OPENSCENARIO::Stop(inRoutePosition_);
}

}  // namespace OPENSCENARIO
