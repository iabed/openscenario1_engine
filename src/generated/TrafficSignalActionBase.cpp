/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSignalActionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "TrafficSignalControllerAction.h"
#include "TrafficSignalStateAction.h"

namespace OPENSCENARIO
{
static TrafficSignalActionBase::TrafficSignalAction_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrafficSignalAction> trafficSignalAction)
{
    if (auto element = trafficSignalAction->GetTrafficSignalControllerAction(); element)
    {
        return Builder::transform<TrafficSignalControllerAction>(element);
    }
    if (auto element = trafficSignalAction->GetTrafficSignalStateAction(); element)
    {
        return Builder::transform<TrafficSignalStateAction>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::ITrafficSignalAction");
}

TrafficSignalActionBase::TrafficSignalActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrafficSignalAction> trafficSignalAction)
    : Node{"TrafficSignalActionBase"}, trafficSignalAction_{(std::cout << "TrafficSignalActionBase instantiating trafficSignalAction_" << std::endl, resolve_choices(trafficSignalAction))}

{
}

bool TrafficSignalActionBase::Complete() const
{
    std::cout << "TrafficSignalActionBase complete?\n";
    return OPENSCENARIO::Complete(trafficSignalAction_);
}

void TrafficSignalActionBase::Step()
{
    std::cout << "TrafficSignalActionBase step!\n";
    OPENSCENARIO::Step(trafficSignalAction_);
}

void TrafficSignalActionBase::Stop()
{
    std::cout << "TrafficSignalActionBase stop!\n";
    OPENSCENARIO::Stop(trafficSignalAction_);
}

}  // namespace OPENSCENARIO
