/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <OpenScenarioParser/openScenarioLib/v1_0/generated/api/ApiClassInterfaces.h>

#include <iostream>
#include <string>
#include <variant>
#include <vector>

#include "Common/Node.h"
#include "Common/StateMachine.h"
#include "Story.h"    // maybe replace with baseclass
#include "Trigger.h"  // maybe replace with baseclass

namespace OPENSCENARIO
{
class StoryboardBase : public Node
{
  public:
    using Stories_t = std::vector<std::unique_ptr<Node>>;

    explicit StoryboardBase(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IStoryboard> storyboard);
    StoryboardBase(StoryboardBase&&) = default;
    ~StoryboardBase() override = default;

    bool Complete() const override;
    void Step() override;
    void Stop() override;
  protected:
    std::unique_ptr<Node> init_;
    std::unique_ptr<Node> stopTrigger_;
    Stories_t stories_;
    StateMachine<Story, Trigger> statemachine_;
    bool initialized_{false};
};

}  // namespace OPENSCENARIO
