/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ScenarioObjectBase.h"

#include "EntityObject.h"
#include "ObjectController.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
ScenarioObjectBase::ScenarioObjectBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IScenarioObject> scenarioObject)
    : Node{"ScenarioObjectBase"}, entityObject_{(std::cout << "ScenarioObjectBase instantiating entityObject_" << std::endl, Builder::transform<EntityObject>(scenarioObject->GetEntityObject()))}, objectController_{(std::cout << "ScenarioObjectBase instantiating objectController_" << std::endl, Builder::transform<ObjectController>(scenarioObject->GetObjectController()))}

{
}

bool ScenarioObjectBase::Complete() const
{
    std::cout << "ScenarioObjectBase complete?\n";
    return OPENSCENARIO::Complete(entityObject_) && OPENSCENARIO::Complete(objectController_);
}

void ScenarioObjectBase::Step()
{
    std::cout << "ScenarioObjectBase step!\n";
    OPENSCENARIO::Step(entityObject_);
    OPENSCENARIO::Step(objectController_);
}

void ScenarioObjectBase::Stop()
{
    std::cout << "ScenarioObjectBase stop!\n";
    OPENSCENARIO::Stop(entityObject_);
    OPENSCENARIO::Stop(objectController_);
}

}  // namespace OPENSCENARIO
