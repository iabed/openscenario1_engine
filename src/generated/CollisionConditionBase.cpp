/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "CollisionConditionBase.h"

#include "ByObjectType.h"
#include "EntityRef.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
static CollisionConditionBase::CollisionCondition_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ICollisionCondition> collisionCondition)
{
    if (auto element = collisionCondition->GetEntityRef(); element)
    {
        return Builder::transform<EntityRef>(element);
    }
    if (auto element = collisionCondition->GetByType(); element)
    {
        return Builder::transform<ByObjectType>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::ICollisionCondition");
}

CollisionConditionBase::CollisionConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ICollisionCondition> collisionCondition,
    mantle_api::IEnvironment& environment)
    : Node{"CollisionConditionBase"}, collisionCondition_{(std::cout << "CollisionConditionBase instantiating collisionCondition_" << std::endl, resolve_choices(collisionCondition))}

      ,
      environment_{environment}
{
}

bool CollisionConditionBase::Complete() const
{
    std::cout << "CollisionConditionBase complete?\n";
    return OPENSCENARIO::Complete(collisionCondition_);
}

void CollisionConditionBase::Step()
{
    std::cout << "CollisionConditionBase step!\n";
    OPENSCENARIO::Step(collisionCondition_);
}

void CollisionConditionBase::Stop()
{
    std::cout << "CollisionConditionBase stop!\n";
    OPENSCENARIO::Stop(collisionCondition_);
}

}  // namespace OPENSCENARIO
