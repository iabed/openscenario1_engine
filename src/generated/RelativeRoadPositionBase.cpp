/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RelativeRoadPositionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Orientation.h"

namespace OPENSCENARIO
{
RelativeRoadPositionBase::RelativeRoadPositionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRelativeRoadPosition> relativeRoadPosition)
    : Node{"RelativeRoadPositionBase"}, orientation_{(std::cout << "RelativeRoadPositionBase instantiating orientation_" << std::endl, Builder::transform<Orientation>(relativeRoadPosition->GetOrientation()))}

{
}

bool RelativeRoadPositionBase::Complete() const
{
    std::cout << "RelativeRoadPositionBase complete?\n";
    return OPENSCENARIO::Complete(orientation_);
}

void RelativeRoadPositionBase::Step()
{
    std::cout << "RelativeRoadPositionBase step!\n";
    OPENSCENARIO::Step(orientation_);
}

void RelativeRoadPositionBase::Stop()
{
    std::cout << "RelativeRoadPositionBase stop!\n";
    OPENSCENARIO::Stop(orientation_);
}

}  // namespace OPENSCENARIO
