/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EntityConditionBase.h"

#include "AccelerationCondition.h"
#include "CollisionCondition.h"
#include "DistanceCondition.h"
#include "EndOfRoadCondition.h"
#include "OffroadCondition.h"
#include "OpenScenarioEngineFactory.h"
#include "ReachPositionCondition.h"
#include "RelativeDistanceCondition.h"
#include "RelativeSpeedCondition.h"
#include "SpeedCondition.h"
#include "StandStillCondition.h"
#include "TimeHeadwayCondition.h"
#include "TimeToCollisionCondition.h"
#include "TraveledDistanceCondition.h"

namespace OPENSCENARIO
{
static EntityConditionBase::EntityCondition_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEntityCondition> entityCondition)
{
    if (auto element = entityCondition->GetEndOfRoadCondition(); element)
    {
        return Builder::transform<EndOfRoadCondition>(element);
    }
    if (auto element = entityCondition->GetCollisionCondition(); element)
    {
        return Builder::transform<CollisionCondition>(element);
    }
    if (auto element = entityCondition->GetOffroadCondition(); element)
    {
        return Builder::transform<OffroadCondition>(element);
    }
    if (auto element = entityCondition->GetTimeHeadwayCondition(); element)
    {
        return Builder::transform<TimeHeadwayCondition>(element);
    }
    if (auto element = entityCondition->GetTimeToCollisionCondition(); element)
    {
        return Builder::transform<TimeToCollisionCondition>(element);
    }
    if (auto element = entityCondition->GetAccelerationCondition(); element)
    {
        return Builder::transform<AccelerationCondition>(element);
    }
    if (auto element = entityCondition->GetStandStillCondition(); element)
    {
        return Builder::transform<StandStillCondition>(element);
    }
    if (auto element = entityCondition->GetSpeedCondition(); element)
    {
        return Builder::transform<SpeedCondition>(element);
    }
    if (auto element = entityCondition->GetRelativeSpeedCondition(); element)
    {
        return Builder::transform<RelativeSpeedCondition>(element);
    }
    if (auto element = entityCondition->GetTraveledDistanceCondition(); element)
    {
        return Builder::transform<TraveledDistanceCondition>(element);
    }
    if (auto element = entityCondition->GetReachPositionCondition(); element)
    {
        return Builder::transform<ReachPositionCondition>(element);
    }
    if (auto element = entityCondition->GetDistanceCondition(); element)
    {
        return Builder::transform<DistanceCondition>(element);
    }
    if (auto element = entityCondition->GetRelativeDistanceCondition(); element)
    {
        return Builder::transform<RelativeDistanceCondition>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IEntityCondition");
}

EntityConditionBase::EntityConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEntityCondition> entityCondition,
    mantle_api::IEnvironment& environment)
    : Node{"EntityConditionBase"}, entityCondition_{(std::cout << "EntityConditionBase instantiating entityCondition_" << std::endl, resolve_choices(entityCondition))}

      ,
      environment_{environment}
{
}

bool EntityConditionBase::Complete() const
{
    std::cout << "EntityConditionBase complete?\n";
    return OPENSCENARIO::Complete(entityCondition_);
}

void EntityConditionBase::Step()
{
    std::cout << "EntityConditionBase step!\n";
    OPENSCENARIO::Step(entityCondition_);
}

void EntityConditionBase::Stop()
{
    std::cout << "EntityConditionBase stop!\n";
    OPENSCENARIO::Stop(entityCondition_);
}

}  // namespace OPENSCENARIO
