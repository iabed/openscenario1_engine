/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TimeToCollisionConditionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "TimeToCollisionConditionTarget.h"

namespace OPENSCENARIO
{
TimeToCollisionConditionBase::TimeToCollisionConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITimeToCollisionCondition> timeToCollisionCondition,
    mantle_api::IEnvironment& environment)
    : Node{"TimeToCollisionConditionBase"}, timeToCollisionConditionTarget_{(std::cout << "TimeToCollisionConditionBase instantiating timeToCollisionConditionTarget_" << std::endl, Builder::transform<TimeToCollisionConditionTarget>(timeToCollisionCondition->GetTimeToCollisionConditionTarget()))}, rule_{Rule::from(timeToCollisionCondition)}, environment_{environment}
{
}

bool TimeToCollisionConditionBase::Complete() const
{
    std::cout << "TimeToCollisionConditionBase complete?\n";
    return OPENSCENARIO::Complete(timeToCollisionConditionTarget_);
}

void TimeToCollisionConditionBase::Step()
{
    std::cout << "TimeToCollisionConditionBase step!\n";
    OPENSCENARIO::Step(timeToCollisionConditionTarget_);
}

void TimeToCollisionConditionBase::Stop()
{
    std::cout << "TimeToCollisionConditionBase stop!\n";
    OPENSCENARIO::Stop(timeToCollisionConditionTarget_);
}

}  // namespace OPENSCENARIO
