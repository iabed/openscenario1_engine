/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "StoryboardBase.h"

#include "Init.h"
#include "OpenScenarioEngineFactory.h"
#include "Story.h"
#include "Trigger.h"

namespace OPENSCENARIO
{
StoryboardBase::StoryboardBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IStoryboard> storyboard)
    : Node{"StoryboardBase"}, init_{(std::cout << "StoryboardBase instantiating init_" << std::endl, Builder::transform<Init>(storyboard->GetInit()))}, stopTrigger_{(std::cout << "StoryboardBase instantiating stopTrigger_" << std::endl, Builder::transform<Trigger>(storyboard->GetStopTrigger()))}, stories_{(std::cout << "StoryboardBase instantiating stories_" << std::endl, Builder::transform<Story, Stories_t>(storyboard, &NET_ASAM_OPENSCENARIO::v1_0::IStoryboard::GetStories))}

      ,
      statemachine_{stories_, nullptr, stopTrigger_.get()}
{
}

bool StoryboardBase::Complete() const
{
    std::cout << "StoryboardBase complete?\n";
    return statemachine_.Complete();
}

void StoryboardBase::Step()
{
    std::cout << "StoryboardBase step!\n";
    if(!initialized_)
    {
        init_->Step();
        initialized_ = true;
    }
    statemachine_.Step();
}

void StoryboardBase::Stop()
{
    std::cout << "StoryboardBase stop!\n";
    init_->Stop();
    statemachine_.Stop();
}

}  // namespace OPENSCENARIO
