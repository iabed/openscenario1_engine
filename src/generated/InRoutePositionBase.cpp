/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "InRoutePositionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "PositionInLaneCoordinates.h"
#include "PositionInRoadCoordinates.h"
#include "PositionOfCurrentEntity.h"

namespace OPENSCENARIO
{
static InRoutePositionBase::InRoutePosition_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IInRoutePosition> inRoutePosition)
{
    if (auto element = inRoutePosition->GetFromCurrentEntity(); element)
    {
        return Builder::transform<PositionOfCurrentEntity>(element);
    }
    if (auto element = inRoutePosition->GetFromRoadCoordinates(); element)
    {
        return Builder::transform<PositionInRoadCoordinates>(element);
    }
    if (auto element = inRoutePosition->GetFromLaneCoordinates(); element)
    {
        return Builder::transform<PositionInLaneCoordinates>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IInRoutePosition");
}

InRoutePositionBase::InRoutePositionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IInRoutePosition> inRoutePosition)
    : Node{"InRoutePositionBase"}, inRoutePosition_{(std::cout << "InRoutePositionBase instantiating inRoutePosition_" << std::endl, resolve_choices(inRoutePosition))}

{
}

bool InRoutePositionBase::Complete() const
{
    std::cout << "InRoutePositionBase complete?\n";
    return OPENSCENARIO::Complete(inRoutePosition_);
}

void InRoutePositionBase::Step()
{
    std::cout << "InRoutePositionBase step!\n";
    OPENSCENARIO::Step(inRoutePosition_);
}

void InRoutePositionBase::Stop()
{
    std::cout << "InRoutePositionBase stop!\n";
    OPENSCENARIO::Stop(inRoutePosition_);
}

}  // namespace OPENSCENARIO
