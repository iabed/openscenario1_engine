/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "OpenScenarioBase.h"

#include "FileHeader.h"
#include "OpenScenarioCategory.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
OpenScenarioBase::OpenScenarioBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IOpenScenario> openScenario)
    : Node{"OpenScenarioBase"}, fileHeader_{(std::cout << "OpenScenarioBase instantiating fileHeader_" << std::endl, Builder::transform<FileHeader>(openScenario->GetFileHeader()))}, openScenarioCategory_{(std::cout << "OpenScenarioBase instantiating openScenarioCategory_" << std::endl, Builder::transform<OpenScenarioCategory>(openScenario->GetOpenScenarioCategory()))}

{
}

bool OpenScenarioBase::Complete() const
{
    std::cout << "OpenScenarioBase complete?\n";
    return OPENSCENARIO::Complete(fileHeader_) && OPENSCENARIO::Complete(openScenarioCategory_);
}

void OpenScenarioBase::Step()
{
    std::cout << "OpenScenarioBase step!\n";
    OPENSCENARIO::Step(fileHeader_);
    OPENSCENARIO::Step(openScenarioCategory_);
}

void OpenScenarioBase::Stop()
{
    std::cout << "OpenScenarioBase stop!\n";
    OPENSCENARIO::Stop(fileHeader_);
    OPENSCENARIO::Stop(openScenarioCategory_);
}

}  // namespace OPENSCENARIO
