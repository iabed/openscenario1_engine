/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LateralActionBase.h"

#include "LaneChangeAction.h"
#include "LaneOffsetAction.h"
#include "LateralDistanceAction.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
static LateralActionBase::LateralAction_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILateralAction> lateralAction)
{
    if (auto element = lateralAction->GetLaneChangeAction(); element)
    {
        return Builder::transform<LaneChangeAction>(element);
    }
    if (auto element = lateralAction->GetLaneOffsetAction(); element)
    {
        return Builder::transform<LaneOffsetAction>(element);
    }
    if (auto element = lateralAction->GetLateralDistanceAction(); element)
    {
        return Builder::transform<LateralDistanceAction>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::ILateralAction");
}

LateralActionBase::LateralActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILateralAction> lateralAction)
    : Node{"LateralActionBase"}, lateralAction_{(std::cout << "LateralActionBase instantiating lateralAction_" << std::endl, resolve_choices(lateralAction))}

{
}

bool LateralActionBase::Complete() const
{
    std::cout << "LateralActionBase complete?\n";
    return OPENSCENARIO::Complete(lateralAction_);
}

void LateralActionBase::Step()
{
    std::cout << "LateralActionBase step!\n";
    OPENSCENARIO::Step(lateralAction_);
}

void LateralActionBase::Stop()
{
    std::cout << "LateralActionBase stop!\n";
    OPENSCENARIO::Stop(lateralAction_);
}

}  // namespace OPENSCENARIO
