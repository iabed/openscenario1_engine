/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SynchronizeActionBase.h"

#include "FinalSpeed.h"
#include "OpenScenarioEngineFactory.h"
#include "Position.h"

namespace OPENSCENARIO
{
SynchronizeActionBase::SynchronizeActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ISynchronizeAction> synchronizeAction)
    : Node{"SynchronizeActionBase"}, targetPositionMaster_{(std::cout << "SynchronizeActionBase instantiating targetPositionMaster_" << std::endl, Builder::transform<Position>(synchronizeAction->GetTargetPositionMaster()))}, targetPosition_{(std::cout << "SynchronizeActionBase instantiating targetPosition_" << std::endl, Builder::transform<Position>(synchronizeAction->GetTargetPosition()))}, finalSpeed_{(std::cout << "SynchronizeActionBase instantiating finalSpeed_" << std::endl, Builder::transform<FinalSpeed>(synchronizeAction->GetFinalSpeed()))}

{
}

bool SynchronizeActionBase::Complete() const
{
    std::cout << "SynchronizeActionBase complete?\n";
    return OPENSCENARIO::Complete(targetPositionMaster_) && OPENSCENARIO::Complete(targetPosition_) && OPENSCENARIO::Complete(finalSpeed_);
}

void SynchronizeActionBase::Step()
{
    std::cout << "SynchronizeActionBase step!\n";
    OPENSCENARIO::Step(targetPositionMaster_);
    OPENSCENARIO::Step(targetPosition_);
    OPENSCENARIO::Step(finalSpeed_);
}

void SynchronizeActionBase::Stop()
{
    std::cout << "SynchronizeActionBase stop!\n";
    OPENSCENARIO::Stop(targetPositionMaster_);
    OPENSCENARIO::Stop(targetPosition_);
    OPENSCENARIO::Stop(finalSpeed_);
}

}  // namespace OPENSCENARIO
