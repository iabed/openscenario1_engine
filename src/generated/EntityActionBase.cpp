/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EntityActionBase.h"

#include "AddEntityAction.h"
#include "DeleteEntityAction.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
static EntityActionBase::EntityAction_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEntityAction> entityAction)
{
    if (auto element = entityAction->GetAddEntityAction(); element)
    {
        return Builder::transform<AddEntityAction>(element);
    }
    if (auto element = entityAction->GetDeleteEntityAction(); element)
    {
        return Builder::transform<DeleteEntityAction>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IEntityAction");
}

EntityActionBase::EntityActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEntityAction> entityAction)
    : Node{"EntityActionBase"}, entityAction_{(std::cout << "EntityActionBase instantiating entityAction_" << std::endl, resolve_choices(entityAction))}

{
}

bool EntityActionBase::Complete() const
{
    std::cout << "EntityActionBase complete?\n";
    return OPENSCENARIO::Complete(entityAction_);
}

void EntityActionBase::Step()
{
    std::cout << "EntityActionBase step!\n";
    OPENSCENARIO::Step(entityAction_);
}

void EntityActionBase::Stop()
{
    std::cout << "EntityActionBase stop!\n";
    OPENSCENARIO::Stop(entityAction_);
}

}  // namespace OPENSCENARIO
