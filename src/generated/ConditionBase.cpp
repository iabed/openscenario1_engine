/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ConditionBase.h"

#include "ByEntityCondition.h"
#include "ByValueCondition.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
static ConditionBase::Condition_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ICondition> condition)
{
    if (auto element = condition->GetByEntityCondition(); element)
    {
        return Builder::transform<ByEntityCondition>(element);
    }
    if (auto element = condition->GetByValueCondition(); element)
    {
        return Builder::transform<ByValueCondition>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::ICondition");
}

ConditionBase::ConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ICondition> condition,
    mantle_api::IEnvironment& environment)
    : Node{"ConditionBase"}, condition_{(std::cout << "ConditionBase instantiating condition_" << std::endl, resolve_choices(condition))}, conditionEdge_{ConditionEdge::from(condition)}, environment_{environment}
{
}

bool ConditionBase::Complete() const
{
    std::cout << "ConditionBase complete?\n";
    return OPENSCENARIO::Complete(condition_);
}

void ConditionBase::Step()
{
    std::cout << "ConditionBase step!\n";
    OPENSCENARIO::Step(condition_);
}

void ConditionBase::Stop()
{
    std::cout << "ConditionBase stop!\n";
    OPENSCENARIO::Stop(condition_);
}

}  // namespace OPENSCENARIO
