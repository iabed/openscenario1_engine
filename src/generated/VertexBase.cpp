/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "VertexBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Position.h"

namespace OPENSCENARIO
{
VertexBase::VertexBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IVertex> vertex)
    : Node{"VertexBase"}, position_{(std::cout << "VertexBase instantiating position_" << std::endl, Builder::transform<Position>(vertex->GetPosition()))}

{
}

bool VertexBase::Complete() const
{
    std::cout << "VertexBase complete?\n";
    return OPENSCENARIO::Complete(position_);
}

void VertexBase::Step()
{
    std::cout << "VertexBase step!\n";
    OPENSCENARIO::Step(position_);
}

void VertexBase::Stop()
{
    std::cout << "VertexBase stop!\n";
    OPENSCENARIO::Stop(position_);
}

}  // namespace OPENSCENARIO
