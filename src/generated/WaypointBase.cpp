/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "WaypointBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Position.h"

namespace OPENSCENARIO
{
WaypointBase::WaypointBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IWaypoint> waypoint)
    : Node{"WaypointBase"}, position_{(std::cout << "WaypointBase instantiating position_" << std::endl, Builder::transform<Position>(waypoint->GetPosition()))}

{
}

bool WaypointBase::Complete() const
{
    std::cout << "WaypointBase complete?\n";
    return OPENSCENARIO::Complete(position_);
}

void WaypointBase::Step()
{
    std::cout << "WaypointBase step!\n";
    OPENSCENARIO::Step(position_);
}

void WaypointBase::Stop()
{
    std::cout << "WaypointBase stop!\n";
    OPENSCENARIO::Stop(position_);
}

}  // namespace OPENSCENARIO
