/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "MiscObjectCatalogLocationBase.h"

#include "Directory.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
MiscObjectCatalogLocationBase::MiscObjectCatalogLocationBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IMiscObjectCatalogLocation> miscObjectCatalogLocation)
    : Node{"MiscObjectCatalogLocationBase"}, directory_{(std::cout << "MiscObjectCatalogLocationBase instantiating directory_" << std::endl, Builder::transform<Directory>(miscObjectCatalogLocation->GetDirectory()))}

{
}

bool MiscObjectCatalogLocationBase::Complete() const
{
    std::cout << "MiscObjectCatalogLocationBase complete?\n";
    return OPENSCENARIO::Complete(directory_);
}

void MiscObjectCatalogLocationBase::Step()
{
    std::cout << "MiscObjectCatalogLocationBase step!\n";
    OPENSCENARIO::Step(directory_);
}

void MiscObjectCatalogLocationBase::Stop()
{
    std::cout << "MiscObjectCatalogLocationBase stop!\n";
    OPENSCENARIO::Stop(directory_);
}

}  // namespace OPENSCENARIO
