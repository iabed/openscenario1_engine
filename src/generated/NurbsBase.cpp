/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "NurbsBase.h"

#include "ControlPoint.h"
#include "Knot.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
NurbsBase::NurbsBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::INurbs> nurbs)
    : Node{"NurbsBase"}, controlPoints_{(std::cout << "NurbsBase instantiating controlPoints_" << std::endl, Builder::transform<ControlPoint, ControlPoints_t>(nurbs, &NET_ASAM_OPENSCENARIO::v1_0::INurbs::GetControlPoints))}, knots_{(std::cout << "NurbsBase instantiating knots_" << std::endl, Builder::transform<Knot, Knots_t>(nurbs, &NET_ASAM_OPENSCENARIO::v1_0::INurbs::GetKnots))}

{
}

bool NurbsBase::Complete() const
{
    std::cout << "NurbsBase complete?\n";
    return OPENSCENARIO::Complete(controlPoints_) && OPENSCENARIO::Complete(knots_);
}

void NurbsBase::Step()
{
    std::cout << "NurbsBase step!\n";
    OPENSCENARIO::Step(controlPoints_);
    OPENSCENARIO::Step(knots_);
}

void NurbsBase::Stop()
{
    std::cout << "NurbsBase stop!\n";
    OPENSCENARIO::Stop(controlPoints_);
    OPENSCENARIO::Stop(knots_);
}

}  // namespace OPENSCENARIO
