/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "PedestrianBase.h"

#include "BoundingBox.h"
#include "OpenScenarioEngineFactory.h"
#include "ParameterDeclaration.h"
#include "Properties.h"

namespace OPENSCENARIO
{
PedestrianBase::PedestrianBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IPedestrian> pedestrian)
    : Node{"PedestrianBase"}, boundingBox_{(std::cout << "PedestrianBase instantiating boundingBox_" << std::endl, Builder::transform<BoundingBox>(pedestrian->GetBoundingBox()))}, properties_{(std::cout << "PedestrianBase instantiating properties_" << std::endl, Builder::transform<Properties>(pedestrian->GetProperties()))}, parameterDeclarations_{(std::cout << "PedestrianBase instantiating parameterDeclarations_" << std::endl, Builder::transform<ParameterDeclaration, ParameterDeclarations_t>(pedestrian, &NET_ASAM_OPENSCENARIO::v1_0::IPedestrian::GetParameterDeclarations))}

{
}

bool PedestrianBase::Complete() const
{
    std::cout << "PedestrianBase complete?\n";
    return OPENSCENARIO::Complete(boundingBox_) && OPENSCENARIO::Complete(properties_) && OPENSCENARIO::Complete(parameterDeclarations_);
}

void PedestrianBase::Step()
{
    std::cout << "PedestrianBase step!\n";
    OPENSCENARIO::Step(boundingBox_);
    OPENSCENARIO::Step(properties_);
    OPENSCENARIO::Step(parameterDeclarations_);
}

void PedestrianBase::Stop()
{
    std::cout << "PedestrianBase stop!\n";
    OPENSCENARIO::Stop(boundingBox_);
    OPENSCENARIO::Stop(properties_);
    OPENSCENARIO::Stop(parameterDeclarations_);
}

}  // namespace OPENSCENARIO
