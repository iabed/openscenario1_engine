/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SimulationTimeConditionBase.h"

#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
SimulationTimeConditionBase::SimulationTimeConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ISimulationTimeCondition> simulationTimeCondition,
    mantle_api::IEnvironment& environment)
    : Node{"SimulationTimeConditionBase"}, rule_{Rule::from(simulationTimeCondition)}, environment_{environment}
{
}

}  // namespace OPENSCENARIO
