/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ShapeBase.h"

#include "Clothoid.h"
#include "Nurbs.h"
#include "OpenScenarioEngineFactory.h"
#include "Polyline.h"

namespace OPENSCENARIO
{
static ShapeBase::Shape_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IShape> shape)
{
    if (auto element = shape->GetPolyline(); element)
    {
        return Builder::transform<Polyline>(element);
    }
    if (auto element = shape->GetClothoid(); element)
    {
        return Builder::transform<Clothoid>(element);
    }
    if (auto element = shape->GetNurbs(); element)
    {
        return Builder::transform<Nurbs>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IShape");
}

ShapeBase::ShapeBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IShape> shape)
    : Node{"ShapeBase"}, shape_{(std::cout << "ShapeBase instantiating shape_" << std::endl, resolve_choices(shape))}

{
}

bool ShapeBase::Complete() const
{
    std::cout << "ShapeBase complete?\n";
    return OPENSCENARIO::Complete(shape_);
}

void ShapeBase::Step()
{
    std::cout << "ShapeBase step!\n";
    OPENSCENARIO::Step(shape_);
}

void ShapeBase::Stop()
{
    std::cout << "ShapeBase stop!\n";
    OPENSCENARIO::Stop(shape_);
}

}  // namespace OPENSCENARIO
