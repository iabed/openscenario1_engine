/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TriggeringEntitiesBase.h"

#include "EntityRef.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
TriggeringEntitiesBase::TriggeringEntitiesBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITriggeringEntities> triggeringEntities)
    : Node{"TriggeringEntitiesBase"}, entityRefs_{(std::cout << "TriggeringEntitiesBase instantiating entityRefs_" << std::endl, Builder::transform<EntityRef, EntityRefs_t>(triggeringEntities, &NET_ASAM_OPENSCENARIO::v1_0::ITriggeringEntities::GetEntityRefs))}

{
}

bool TriggeringEntitiesBase::Complete() const
{
    std::cout << "TriggeringEntitiesBase complete?\n";
    return OPENSCENARIO::Complete(entityRefs_);
}

void TriggeringEntitiesBase::Step()
{
    std::cout << "TriggeringEntitiesBase step!\n";
    OPENSCENARIO::Step(entityRefs_);
}

void TriggeringEntitiesBase::Stop()
{
    std::cout << "TriggeringEntitiesBase stop!\n";
    OPENSCENARIO::Stop(entityRefs_);
}

}  // namespace OPENSCENARIO
