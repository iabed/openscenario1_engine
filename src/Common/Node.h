/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <iostream>

namespace OPENSCENARIO
{
class Node
{
  public:
    Node(std::string name)
        : name_{std::move(name)}
    {
        std::cout << "created " << name_ << std::endl;
    }

    Node(Node&&) = default;

    virtual ~Node() = default;

    virtual void Step()
    {
        std::cout << "step! " << name_ << std::endl;
    }

    virtual void Stop()
    {
        std::cout << "stop! " << name_ << std::endl;
    }

    virtual bool Complete() const
    {
        std::cout << "complete? (always)" << name_ << std::endl;
        return true;
    }

    virtual bool IsSatisfied() const
    {
        std::cout << "satisfied? (always)" << name_ << std::endl;
        return true;
    }

  protected:
    const std::string name_;
};

[[nodiscard]] inline bool Complete(const std::unique_ptr<Node>& node)
{
    return node->Complete();
}

[[nodiscard]] inline bool Complete(const std::vector<std::unique_ptr<Node>>& nodes)
{
    return std::all_of(nodes.begin(), nodes.end(), [](const auto& node) { return node->Complete(); });
}

inline void Step(std::unique_ptr<Node>& node)
{
    node->Step();
}

inline void Step(std::vector<std::unique_ptr<Node>>& nodes)
{
    std::for_each(nodes.begin(), nodes.end(), [](auto& node) { return node->Step(); });
}

inline void Stop(std::unique_ptr<Node>& node)
{
    node->Stop();
}

inline void Stop(std::vector<std::unique_ptr<Node>>& nodes)
{
    std::for_each(nodes.begin(), nodes.end(), [](auto& node) { return node->Stop(); });
}

}  // namespace OPENSCENARIO
