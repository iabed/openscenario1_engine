/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "AcquirePositionAction.h"

namespace OPENSCENARIO
{

bool AcquirePositionAction::Complete() const
{
    std::cout << "AcquirePositionAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void AcquirePositionAction::Step()
{
    std::cout << "AcquirePositionAction::Step() not implemented yet. Doing nothing.\n";
}

void AcquirePositionAction::Stop()
{
    std::cout << "AcquirePositionAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO