/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "AssignRouteAction.h"

namespace OPENSCENARIO
{

bool AssignRouteAction::Complete() const
{
    std::cout << "AssignRouteAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void AssignRouteAction::Step()
{
    std::cout << "AssignRouteAction::Step() not implemented yet. Doing nothing.\n";
}

void AssignRouteAction::Stop()
{
    std::cout << "AssignRouteAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO