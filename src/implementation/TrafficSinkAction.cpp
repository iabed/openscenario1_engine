/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSinkAction.h"

namespace OPENSCENARIO
{

bool TrafficSinkAction::Complete() const
{
    std::cout << "TrafficSinkAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void TrafficSinkAction::Step()
{
    std::cout << "TrafficSinkAction::Step() not implemented yet. Doing nothing.\n";
}

void TrafficSinkAction::Stop()
{
    std::cout << "TrafficSinkAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO