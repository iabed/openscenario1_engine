/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "generated/DeleteEntityActionBase.h"

namespace OPENSCENARIO
{
class DeleteEntityAction : public DeleteEntityActionBase
{
  public:
    explicit DeleteEntityAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IDeleteEntityAction> parsedDeleteEntityAction)
        : DeleteEntityActionBase(parsedDeleteEntityAction)
    {
    }
    DeleteEntityAction(DeleteEntityAction&&) = default;
    ~DeleteEntityAction() override = default;

    bool Complete() const override;
    void Step() override;
    void Stop() override;
};

}  // namespace OPENSCENARIO
