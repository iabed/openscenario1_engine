/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "generated/OverrideSteeringWheelActionBase.h"

namespace OPENSCENARIO
{
class OverrideSteeringWheelAction : public OverrideSteeringWheelActionBase
{
  public:
    explicit OverrideSteeringWheelAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IOverrideSteeringWheelAction> parsedOverrideSteeringWheelAction)
        : OverrideSteeringWheelActionBase(parsedOverrideSteeringWheelAction)
    {
    }
    OverrideSteeringWheelAction(OverrideSteeringWheelAction&&) = default;
    ~OverrideSteeringWheelAction() override = default;

    bool Complete() const override;
    void Step() override;
    void Stop() override;
};

}  // namespace OPENSCENARIO
