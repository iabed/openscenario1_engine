/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LongitudinalDistanceAction.h"

namespace OPENSCENARIO
{

bool LongitudinalDistanceAction::Complete() const
{
    std::cout << "LongitudinalDistanceAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void LongitudinalDistanceAction::Step()
{
    std::cout << "LongitudinalDistanceAction::Step() not implemented yet. Doing nothing.\n";
}

void LongitudinalDistanceAction::Stop()
{
    std::cout << "LongitudinalDistanceAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO