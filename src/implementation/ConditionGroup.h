/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "generated/ConditionGroupBase.h"

namespace OPENSCENARIO
{
class ConditionGroup : public ConditionGroupBase
{
  public:
    explicit ConditionGroup(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IConditionGroup> parsedConditionGroup,
        mantle_api::IEnvironment& environment)
        :  ConditionGroupBase(parsedConditionGroup, environment)
    {
    }

    /// @brief Checks if association of Conditions is satisfied
    ///        according to OpenSCENARIO User Guide Version 1.0.0
    /// @returns true if and only if all associated Conditions are true
    bool IsSatisfied() const override;
};

}  // namespace OPENSCENARIO
