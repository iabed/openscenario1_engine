/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSourceAction.h"

namespace OPENSCENARIO
{

bool TrafficSourceAction::Complete() const
{
    std::cout << "TrafficSourceAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void TrafficSourceAction::Step()
{
    std::cout << "TrafficSourceAction::Step() not implemented yet. Doing nothing.\n";
}

void TrafficSourceAction::Stop()
{
    std::cout << "TrafficSourceAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO