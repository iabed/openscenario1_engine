/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Trigger.h"

bool OPENSCENARIO::Trigger::IsSatisfied() const
{
    std::cout << "Trigger::IsSatisfied?\n";
    return std::any_of(conditionGroups_.begin(), conditionGroups_.end(), [](auto& c) { return c->IsSatisfied(); });
}
